@echo off
echo Welcome and thanks for being part of the EKS Workshop!
echo Please wait while the software is installed!
echo ---

echo Checking chocolatey is installed
choco --version

echo Installing Terraform v0.11.13
choco install terraform --version 0.11.13 -y

echo Checking Terraform installation
cmd.exe /c "call RefreshEnv.cmd"
terraform -version
echo %errorlevel%

echo Installing awscli
choco install awscli -y

echo Checking awscli installation
cmd.exe /c "call RefreshEnv.cmd"
aws --version
echo %errorlevel%

echo Installing Kubectl
choco install kubernetes-cli --version 1.12.1 -y

echo Checking Kubectl installation
cmd.exe /c "call RefreshEnv.cmd"
kubectl version
echo %errorlevel%

echo ...
echo "It seems everything's Ok!"

echo ...
echo Let's check versions again
terraform -version
echo %errorlevel%
aws --version
echo %errorlevel%
kubectl version
echo %errorlevel%

timeout 500
