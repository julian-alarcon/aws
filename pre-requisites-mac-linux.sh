#!/usr/bin/env bash

BASE_DIR="/opt/eks_workshop"
TERRAFORM_DIR="$BASE_DIR/terraform/"
KUBECTL_DIR="$BASE_DIR/kubectl/"

TERRAFORM_URL=""
KUBECTL_URL=""

TERRAFORM_LINUX_ZIP="terraform_0.11.14_linux_amd64.zip"
TERRAFORM_MAC_ZIP="terraform_0.11.14_darwin_amd64.zip"
TERRAFORM_ZIP=""


echo "Welcome and thanks for being part of the EKS Workshop!"
echo "Please wait while the software is installed!"
echo "---"

case "$(uname -s)" in
   Darwin)  
	TERRAFORM_URL="https://releases.hashicorp.com/terraform/0.11.14/$TERRAFORM_MAC_ZIP"	
	TERRAFORM_ZIP="$TERRAFORM_MAC_ZIP"
	KUBECTL_URL="https://amazon-eks.s3-us-west-2.amazonaws.com/1.12.9/2019-06-21/bin/darwin/amd64/kubectl"
    ;;

   Linux)     
	TERRAFORM_URL="https://releases.hashicorp.com/terraform/0.11.14/$TERRAFORM_LINUX_ZIP"
	TERRAFORM_ZIP="$TERRAFORM_LINUX_ZIP"
	KUBECTL_URL="https://amazon-eks.s3-us-west-2.amazonaws.com/1.12.9/2019-06-21/bin/linux/amd64/kubectl"
    ;;

   *)     
	echo "Sorry, you aren't using a Linux or Darwin (MacOS) system."
	sleep 10
	exit 1
    ;;
esac

echo "Configuring your workspace"
mkdir -p "$TERRAFORM_DIR"
mkdir -p "$KUBECTL_DIR"
echo ""

echo "Installing terraform"
pushd "$TERRAFORM_DIR"
wget $TERRAFORM_URL
unzip $TERRAFORM_ZIP
export PATH="$PATH:$TERRAFORM_DIR"
popd
echo ""

echo "Installing kubectl"
pushd "$KUBECTL_DIR"
curl -o kubectl $KUBECTL_URL
chmod +x ./kubectl
export PATH="$PATH:$KUBECTL_DIR"
popd
echo ""

echo "Installing aws-cli"
which pip
if [[ $? -eq 0 ]]; then
pip install awscli
else
pip3 install awscli
fi
echo ""

echo "Checking versions of your packages"
echo "Terraform"
terraform -version
if [[ $? -gt 0 ]]; then
echo "ERROR!!!!!!!!!!!!!"
echo "ERROR!!!!!!!!!!!!!"
echo "You have an error with Terraform installation, please follow the guide or contact us via slack (Andres Scarpetta, Maria Camila Pena or Zamir Pena)"
echo "ERROR!!!!!!!!!!!!!"
echo "ERROR!!!!!!!!!!!!!"
fi
echo ""

echo "aws-cli"
aws --version
if [[ $? -gt 0 ]]; then
echo "ERROR!!!!!!!!!!!!!"
echo "ERROR!!!!!!!!!!!!!"
echo "You have an error with AWS-CLI installation, please follow the guide or contact us via slack (Andres Scarpetta, Maria Camila Pena or Zamir Pena)"
echo "ERROR!!!!!!!!!!!!!"
echo "ERROR!!!!!!!!!!!!!"
fi
echo ""

echo "Kubectl"
kubectl
if [[ $? -gt 0 ]]; then
echo "ERROR!!!!!!!!!!!!!"
echo "ERROR!!!!!!!!!!!!!"
echo "You have an error with Kubectl installation, please follow the guide or contact us via slack (Andres Scarpetta, Maria Camila Pena or Zamir Pena)"
echo "ERROR!!!!!!!!!!!!!"
echo "ERROR!!!!!!!!!!!!!"
fi
echo ""
