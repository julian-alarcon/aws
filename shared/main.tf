module "network" {
  source = "./network"  
  
  vpc-cidr-block = "10.0.0.0/16"

  eks-cidr-blocks = ["10.0.1.0/24", "10.0.2.0/24", "10.0.3.0/24"]

  cluster-name = "${var.eks-workshop-name}" 
}

module "eks" {
  source = "./eks"

  cluster-name = "${var.eks-workshop-name}" 

  eks-sg = "${module.network.security-groups-id}"

  eks-subnets = ["${module.network.subnets-ids}"]
  
}
module "worker-node" {
  source = "./worker-node"
  
  eks-version = "${module.eks.eks-version}"

  subnets-ids = ["${module.network.subnets-ids}"]

  cluster-name = "${var.eks-workshop-name}" 

  eks-endpoint = "${module.eks.eks-endpoint}"

  eks-certificate-authority = "${module.eks.eks-certificate-authority}"

  eks-worker-sg-id = ["${module.network.worker-security-groups-id}"]
}


