variable "cluster-name" {
  type = "string"
}
variable "eks-sg" {
  type = "string"
}
variable "eks-subnets" {
  type = "list"
}

