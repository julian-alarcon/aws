output "eks-version" {
  value = "${aws_eks_cluster.eks-cluster.version}"
}
output "eks-endpoint" {
  value = "${aws_eks_cluster.eks-cluster.endpoint}"
}
output "eks-certificate-authority" {
  value = "${aws_eks_cluster.eks-cluster.certificate_authority.0.data}"
}



