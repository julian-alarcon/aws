data "aws_ami" "eks-worker" {
  filter {
    name   = "name"
    values = ["amazon-eks-node-${var.eks-version}-v*"]
  }

  most_recent = true
  owners      = ["602401143452"] # Amazon EKS AMI Account ID
}
data "terraform_remote_state" "eks-node-iam"{
    backend = "s3"
    config = {
      bucket  = "cloud-kube-tfstates"
      key     = "global/iam/terraform.tfstate"
      region  = "us-east-1"
      encrypt = true
    }
}