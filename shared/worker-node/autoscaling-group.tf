resource "aws_autoscaling_group" "demo" {
  desired_capacity     = 1
  launch_configuration = "${aws_launch_configuration.eks-worker-node-launch-config.id}"
  max_size             = 1
  min_size             = 1
  name                 = "${format("%s-terraform-eks-worker", var.cluster-name)}"
  vpc_zone_identifier  = ["${var.subnets-ids}"]

  tag {
    key                 = "Name"
    value               = "${format("%s-terraform-eks-worker", var.cluster-name)}"
    propagate_at_launch = true
  }

  tag {
    key                 = "kubernetes.io/cluster/${var.cluster-name}"
    value               = "owned"
    propagate_at_launch = true
  }
}
