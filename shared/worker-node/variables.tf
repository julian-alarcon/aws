variable "eks-version" {
  type = "string"
}
variable "subnets-ids" {
  type = "list"
}
variable "cluster-name" {
  type = "string"
}
variable "eks-endpoint" {
  type = "string"
}
variable "eks-certificate-authority" {
  type = "string"
}
variable "eks-worker-sg-id" {
  type = "list"
}
variable "instance-key" {
  type = "string"
  default = "EKS-Workshop-key"
}




