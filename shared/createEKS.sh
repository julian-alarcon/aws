#!/bin/bash

echo EKS cluster name:
read eks_cluster_name
terraform init \
  --backend-config="bucket=cloud-kube-tfstates" \
  --backend-config="key=$eks_cluster_name/terraform.tfstate" \
  --backend-config="region=us-east-1"
terraform apply -var eks-workshop-name=$eks_cluster_name
aws eks update-kubeconfig --name $eks_cluster_name
terraform output eks-kubernetes-connection > kubernetesConfigMapWorker.yaml
kubectl apply -f kubernetesConfigMapWorker.yaml
