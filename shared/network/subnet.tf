resource "aws_subnet" "eks-subnet" {

    count = "${length(var.eks-subnet-names)}"
    vpc_id = "${aws_vpc.eks-vpc.id}"
    availability_zone = "${var.availability-zone[count.index]}"
    cidr_block = "${element(var.eks-cidr-blocks, count.index)}"
	 
	tags = "${merge(
		map(
			"Name", "terraform-eks-demo-node",
			"kubernetes.io/cluster/${var.cluster-name}", "shared",
        ),
		map(
			"Name", "terraform-eks-demo-node-internal-elb",
			"kubernetes.io/role/internal-elb", "1",
        ),
		map(
			"Name", "terraform-eks-demo-node-external-elb",
			"kubernetes.io/role/elb", "1",
        )
	)}"
}