resource "aws_security_group" "eks-sg" {
  name        = "${format("%s-terraform-eks-cluster-sg", var.cluster-name)}"
  description = "Cluster communication with worker nodes"
  vpc_id      = "${aws_vpc.eks-vpc.id}"

  egress {
    from_port   = 0
    to_port     = 0
    protocol    = "-1"
    cidr_blocks = ["0.0.0.0/0"]
  }
}
