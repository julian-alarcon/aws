output "security-groups-id" {
  value = "${aws_security_group.eks-sg.id}"
}
output "subnets-ids" {
  value = ["${aws_subnet.eks-subnet.*.id}"]
}
output "worker-security-groups-id" {
  value = "${aws_security_group.eks-worker-node-sg.id}"
}


