resource "aws_vpc" "eks-vpc" {
  cidr_block = "${var.vpc-cidr-block}"

  tags = "${
    map(
     "Name", "terraform-eks-demo-node",
     "kubernetes.io/cluster/${var.cluster-name}", "shared",
    )
  }"

}