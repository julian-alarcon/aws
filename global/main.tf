terraform {
  backend "s3" {
    bucket  = "cloud-kube-tfstates"
    key     = "global/iam/terraform.tfstate"
    region  = "us-east-1"
    encrypt = true
  }
}

module "eks-master" {
  source = "./eks-master"
}
module "eks-worker-node"{
  source = "./eks-worker-node"
}
