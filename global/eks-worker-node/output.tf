output "eks-node-instance-profile-name" {
  value = "${aws_iam_instance_profile.eks-node.name}"
}
output "eks-node-iam-role-arn" {
  value = "${aws_iam_role.eks-node.arn}"
}
