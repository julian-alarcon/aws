# Amazon EKS Workshop

Here, you going to find the requirements you need to start with the workshop.

For Windows users, please run the following command on Powershell to execute scripts on it, this should be run opening the *Powershell console as Administrator*:

```powershell
set-executionpolicy remotesigned
```

More information [here](https://superuser.com/questions/106360/how-to-enable-execution-of-powershell-scripts).

## Install Terraform

For this workshop we are going to deploy the infrastructure using Terraform, an Infrastructure as Code tool.

Please install the 0.11.14 version following the next instructions:

### Windows users:

Go to [Terraform v0.11.14 Binaries](https://releases.hashicorp.com/terraform/0.11.14/) and download the binary according to the operating system you going to use. Depending of the O.S., there's several ways to install Terraform:

After you download the .zip file, extract it and copy the folder's path where Terraform's binary are located. Then open the environment variables settings and add to the PATH variable (both system and user for safety). Then open a terminal and type `terraform`, press enter and you will an output like this:

```
Usage: terraform [-version] [-help] <command> [args]

The available commands for execution are listed below.
The most common, useful commands are shown first, followed by
less common or more advanced commands. If you're just getting
started with Terraform, stick with the common commands. For the
other commands, please read the help and docs before usage.

Common commands:
    apply              Builds or changes infrastructure
    console            Interactive console for Terraform interpolations
    destroy            Destroy Terraform-managed infrastructure
    env                Workspace management
    fmt                Rewrites config files to canonical format
    get                Download and install modules for the configuration
    graph              Create a visual graph of Terraform resources
    import             Import existing infrastructure into Terraform
    init               Initialize a Terraform working directory
    output             Read an output from a state file
    plan               Generate and show an execution plan
    providers          Prints a tree of the providers used in the configuration
    push               Upload this Terraform module to Atlas to run
    refresh            Update local state file against real resources
    show               Inspect Terraform state or plan
    taint              Manually mark a resource for recreation
    untaint            Manually unmark a resource as tainted
    validate           Validates the Terraform files
    version            Prints the Terraform version
    workspace          Workspace management

All other commands:
    0.12checklist      Checks whether the configuration is ready for Terraform v0.12
    debug              Debug output management (experimental)
    force-unlock       Manually unlock the terraform state
    state              Advanced state management
```

### Linux/Mac OS users:

Get the Terraform binaries using the next commands, the first for Linux users and the second for MacOS users:

```bash
wget https://releases.hashicorp.com/terraform/0.11.14/terraform_0.11.14_linux_amd64.zip
```

```bash
wget https://releases.hashicorp.com/terraform/0.11.14/terraform_0.11.14_darwin_amd64.zip
```

Then, unzip the binary:

```bash
unzip terraform_0.11.14_linux_amd64.zip
```

```bash
unzip terraform_0.11.14_darwin_amd64.zip
```

After, open the .profile (bash_profile on MacOS) on the root folder and add:

```bash
export PATH="$PATH:<path where the binary was unzip>"
```

Apply the changes using the `source` command over the .profile file. Then, run `terraform --version` to check the installation.

For more information go to [this official tutorial](https://learn.hashicorp.com/terraform/getting-started/install.html).

## Install AWS CLI

This workshop needs AWS CLI (Amazon Web Services Command Line Interface), which allow you to interact with the AWS resources. Here is the instalation guide for Windows, Linux and MacOS.

### Install the AWS CLI in Windows using MSI Installer

Let's start with the MSI Installer. Just download the MSI and follow the instrutions, with a link bellow to the official documentation.

> [Install the AWS CLI on Windows - Using the MSI Installer](https://docs.aws.amazon.com/cli/latest/userguide/install-windows.html#install-msi-on-windows)

### Install the AWS CLI in Windows, MacOS and Linux using `Python` and `pip` 

First, you need `Python` and `pip` installed for this way to install `AWS CLI`. [Here is a tutorial about the `Python` and `Pip` installation for Windows](https://phoenixnap.com/kb/how-to-install-python-3-windows); [here is a tutorial for Linux](https://docs.python-guide.org/starting/install3/linux/) and [MacOS](https://wsvincent.com/install-python3-mac/) To check if you have `Python` and `pip`, on the console type:

```bash
py --version (for Windows) or python3 (for Windows, Linux and MacOS)
pip --version or pip3 --version (for Windows, Linux and MacOS)
```

The outcome must be something like this:

```bash
Python 3.7.2

pip 19.0.2 from path/to/pip (python 3.7)
```

Then, install the cli using the next command:

```bash
pip install awscli or pip3 install awscli
```

Verify the installation using the next command and the outcome must look like something like this: 

```bash
aws --version
```

```bash
aws-cli/1.16.102 Python/3.6.0 Windows/10 botocore/1.12.92
```

Here is a [link](https://docs.aws.amazon.com/cli/latest/userguide/install-windows.html#awscli-install-windows-pip) to go to the official documentation for the installation using `pip` on Windows, [here](https://docs.aws.amazon.com/cli/latest/userguide/install-macos.html#awscli-install-osx-pip) on MacOS and [here](https://docs.aws.amazon.com/cli/latest/userguide/install-linux.html) in Linux.

But, there is a step you must do: configure the PATH in Windows. First, you need to know there is the binary for aws, so using `where` command you going to see the path to it:

```path
where aws
```

Copy the output and then open the environment variables settings and add the copied path to the PATH variable (both system and user for safety).

Go to the official [documentation](https://docs.aws.amazon.com/cli/latest/userguide/install-windows.html#awscli-install-windows-path) of a deep dive on this step.

## Install kubectl

To install kubectl on Windows, MacOS and Linux, follow the next instructions:

On Windows execute:

```bash
curl -o kubectl.exe https://amazon-eks.s3-us-west-2.amazonaws.com/1.12.9/2019-06-21/bin/windows/amd64/kubectl.exe
```

On MacOS execute:

```bash
curl -o kubectl https://amazon-eks.s3-us-west-2.amazonaws.com/1.12.9/2019-06-21/bin/darwin/amd64/kubectl
```

On Linux execute:

```bash
curl -o kubectl https://amazon-eks.s3-us-west-2.amazonaws.com/1.12.9/2019-06-21/bin/linux/amd64/kubectl
```

Now, let's configure the `PATH`:

On Windows: copy the path where the binary is and then open the environment variables settings and add the copied path to the PATH variable (both system and user for safety).

On MacOS and Linux: 

Give execute permission to the kubectl binary:

```bash
chmod +x ./kubectl
```

Copy the folder to the PATH (Recommendation: create a folder to host kubectl binary):

```bash
export PATH=path/to/the/binary:$PATH
```

Then, check the instalation with this command:

```bash
kubectl version
```

The output can look like this:

```bash
Client Version: version.Info{Major:"1", Minor:"12", GitVersion:"v1.12.7", GitCommit:"6f482974b76db3f1e0f5d24605a9d1d38fad9a2b", GitTreeState:"clean", BuildDate:"2019-03-27T15:15:05Z", GoVersion:"go1.10.8", Compiler:"gc", Platform:"windows/amd64"}
```
